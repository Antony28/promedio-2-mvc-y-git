﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Menu_Controlador : MonoBehaviour
{

    Menu_Modelo m_Modelo;
    Menu_Vista m_Vista;

    void Start()
    {
        m_Modelo = GetComponent<Menu_Modelo>();
        m_Vista = GetComponent<Menu_Vista>();
    }
    void Update()
    {
        m_Vista.SelectOption();
        UpOption();
        DownOption();
    }
    void UpOption()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (m_Modelo.index < m_Modelo.totalOptions - 1)
            {
                m_Modelo.index++;
                m_Vista.MoveDown();
            }
        }
    }
    void DownOption()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (m_Modelo.index > 0)
            {
                m_Modelo.index--;
                m_Vista.MoveUp();
            }
        }
    }
}
