﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu_Vista : MonoBehaviour
{
    Menu_Modelo m_Modelo;

    void Start()
    {
        m_Modelo = GetComponent<Menu_Modelo>();
    }

    public void SelectOption()
     {
        if (Input.GetKeyDown(KeyCode.Space))
         {
            if (m_Modelo.index == 0)
            {
                Debug.Log("Jugar");
            }
            else if (m_Modelo.index == 1)
            {
                Debug.Log("Opciones");
            }
            else if (m_Modelo.index == 2)
            {
                Debug.Log("Creditos");
            }
             else if (m_Modelo.index == 3)
            {
                Debug.Log("Salir");
            }
         }
     }

    public void MoveDown()
    {
        Vector2 position = transform.position;
        position.y -= m_Modelo.yOffset;
        transform.position = position;
    }

    public void MoveUp()
    {
        Vector2 position = transform.position;
        position.y += m_Modelo.yOffset;
        transform.position = position;
    }
}
